import Contact from "./Component/Contact";
import Education from "./Component/Education";
import Main from "./Component/Main";
import Navbar from "./Component/Navbar";
import Project from "./Component/Project";
import Skill from "./Component/Skill";
import Blog from './Component/Blog'
import About from "./Component/About";
import Footer from "./Component/Footer";
export default function App() {
   //bg-[url('https://images.unsplash.com/photo-1480506132288-68f7705954bd?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8cG9ydGZvbGlvJTIwYmFja2dyb3VuZHxlbnwwfHwwfHx8MA%3D%3D')]
   return (
      <div className="sm:mt-[4vh] ">
         <div className="absolute">
            <Main />
         </div>
         <div className="relative">
            <Navbar />
         </div>
         <div className="min-h-screen flex flex-col gap-[5vw]  sm:ml-[30vw] lg:ml-[25vw] mr-[6vw] ">
            <div className=" ">
            </div>
            <div className=" flex flex-col -mt-24    bg-cover ">
               <About />
            </div>
            <div className="relative" id="Education" >
               <Education />
            </div>
            <div className="relative flex flex-col gap-[4vw]   rounded-lg   " id="Skill" >
               <Skill />
            </div>
            <div className="relative" id="Project">
               <Project />
            </div>
            <div className="relative" id="Blog">
               <Blog />
            </div>
            <div className="relative" id="Contact">
               <Contact />
            </div>



         </div>
         <div >
            <Footer />
         </div>
      </div>

   )
}
