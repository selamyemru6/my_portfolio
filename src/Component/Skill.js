
import React, { useEffect, useState } from 'react';
import { fetchSkill, fetchSkillWithCategory, skillCatagory } from './FetchProject';

function Skill() {
  const [categories, setCategory] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState([]);
  const [title, setTitle] = useState(false);

  const handleCategoryChange = async (e) => {
    const category = e.target.value;
    if (category === 'All') {
      const data = await fetchSkill();
      setSelectedCategory(data);
    } else {
      const catData = await fetchSkillWithCategory(category);
      setSelectedCategory(catData);
      setTitle(!title);
    }
  };

  useEffect(() => {
    async function fetchData() {
      const categoryData = await skillCatagory();
      setCategory(categoryData);
      const skillData = await fetchSkill();
      setSelectedCategory(skillData);
    }
    fetchData();
  }, []);
  return (
    <div className=''>
      <h1 className='text-2xl font-bold mb-4 text-[#2c2c23]'>Skill</h1>
      <p className=''>
        As a web developer and Graphics Designer, I have the following skill
      </p>
      <section className='md:gap-40 mt-[2%] '>
        <div className='flex justify-end items-center  '>
          <select 
            className=' cursor-pointer text-semibold items-center py-1 border-b-2 border-gray-300 bg-gray-200 bg-opacity-40 px-3 rounded-lg outline-none hover:bg-black/60 hover:text-white '
            onChange={handleCategoryChange}
            title='All'
            name='All'
          >
            <option value='All'>All</option>
            {Array.isArray(categories) &&
              categories.map((cat, index) => {
                return <option key={cat}>{cat}</option>;
              })}
          </select>
        </div>
        <section className='grid grid-cols-2 sm:grid-cols-3  lg:grid-cols-4  gap-y-11 gap-x-16 mt-10 '>
          {selectedCategory.map((catData) => (
            <div key={catData.$id} className='flex flex-col gap-2'>
              <div className='text-sm font-semibold'>{catData.skill}</div>
              {catData==="Soft_Skill"? "":
               <div className='flex gap-4 items-center'>
               <div className='hover:border hover:border-[#434242] hover:p-2 ' >
                 <img src={catData.devIcons} alt='' width={25} height={25} />
               </div>  
               <div className='flex gap-1'>
                 <img src={catData.icons} alt='' width={10} className='h-[1vw] rotate-45' />
                 <img src={catData.icon1} alt='' width={10} className='h-[1vw] rotate-45' />
                 <img src={catData.icon2} alt='' width={10} className='h-[1vw] rotate-45' />
                 <img src={catData.icon3} alt='' width={10} className='h-[1vw] rotate-45' />
                 <img src={catData.icon4} alt='' width={10} className='h-[1vw] rotate-45' />
               </div>
             </div>}
             
            </div>
          ))}
        </section>
        <div></div>
      </section>
    </div>
  );
}
export default Skill;

