import React, { useState } from 'react'
import { FaCaretDown   } from 'react-icons/fa'
import { Link } from 'react-scroll'
import { FaArrowRight } from 'react-icons/fa'
function Education() {
  const [isOpen,setIsOpen]=useState(false)
  const [isOpen2,setIsOpen2]=useState(false)
  const [isSchool,setIsSchool]=useState(false)
  const toggle=()=>{
    setIsOpen(!isOpen)
  }
  const toggle2=()=>{
    setIsOpen2(!isOpen2)
  }
  const school=()=>{
    setIsSchool(!isSchool)
  }
  return (
    <div>
      <section className=" flex flex-col  mb-[8vh] items-center ">
          <h1 className="text-4xl font-bold mb-4 text-blue-900">Welcome to My Portfolio</h1>
          <p className="text-lg mb-8">Explore my latest projects and get in touch with me.</p>
          <Link to='Contact' className=' flex px-4 py-1.5 rounded-lg bg-gray-100 w-fit items-center text-black animate  hover:bg-black/70 hover:text-white border shadow-3xl  gap-2 shadow-2xl  '><FaArrowRight size={10} />Get in Touch</Link>
        </section>
    <section className=' flex flex-col gap-2  rounded-lg  border  w-full border-gray-300 px-5  shadow-2xl py-8   min-h-[32vh]'>  
      <h1 className='text-2xl  mb-3 font-semibold font-display h-full text-[#321a1a] '>Education</h1>
      <div className='flex border p-4 justify-between  bg-gray-100  rounded-lg  '>
        Bachlor of Degree in Software Engineering at Addis Ababa University.
        <button onClick={toggle}><FaCaretDown size={14}  title='See Education Description'/></button> 
        </div>
       {isOpen&&(
        <div className='
        p-4 bg-gray-200   rounded-lg mt-0'>When choosing a medium yellow color that shows well on a white background, you can use the hexadecimal code #FFCC00. This shade of yellow is slightly darker than a pure yellow and provides good contrast against a white background. Here's an example of how you can use this color code in your CSS:
        </div>
       )}
      <div className='flex  justify-between  border p-4 bg-gray-100 rounded-lg '>
        Graphics Design at Brihanena Selam Printind and Design College.
        <button onClick={toggle2}><FaCaretDown size={14}   title='See Education Description'/></button> 
        </div>
       {isOpen2&&(
        <div className='
        p-4 bg-gray-200 rounded-lg'>When choosing a medium yellow color that shows well on a white background, you can use the hexadecimal code #FFCC00. This shade of yellow is slightly darker than a pure yellow and provides good contrast against a white background. Here's an example of how you can use this color code in your CSS:</div>

       )}
      <div className='flex justify-between border p-4 bg-gray-100 rounded-lg  '>
        Preparatory and Secondary School.
        <button onClick={school}><FaCaretDown  title='See Education Description' size={14} /></button> 
        </div>
       {isSchool&&(
        <div className='
        p-4 bg-gray-200 rounded-lg'>When choosing a medium yellow color that shows well on a white background, you can use the hexadecimal code #FFCC00. This shade of yellow is slightly darker than a pure yellow and provides good contrast against a white background. Here's an example of how you can use this color code in your CSS:</div>
       )}
    </section>
    </div>
  )
}

export default Education
