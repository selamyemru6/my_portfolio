import React, { useState, useRef } from 'react'
import emailjs from "@emailjs/browser"
import { FaEnvelope, FaPhone, FaGithub, FaLinkedin, FaGitlab } from 'react-icons/fa';
function Contact() {
  const form = useRef();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [stateMessage, setStateMessage] = useState(null);
  const sendEmail = (e) => {
    e.persist();
    e.preventDefault();
    setIsSubmitting(true);
    emailjs
      .sendForm("service_xj69a3r", "template_msnqv13", form.current, {
        publicKey: "NzxW0xF9Jn6hkvTII"
      }
        // process.env.REACT_APP_SERVICE_ID,
        // process.env.REACT_APP_TEMPLATE_ID,
        // e.target,
        // process.env.REACT_APP_PUBLIC_KEY
      )
      .then(
        (result) => {
          setStateMessage('Message sent!');
          setIsSubmitting(true);
          setTimeout(() => {
            setStateMessage(null);
            setIsSubmitting(false)
          }, 5000); // hide message after 5 seconds
        },
        (error) => {
          setStateMessage('Something went wrong, please try again later');
          setIsSubmitting(true);
          setTimeout(() => {
            setStateMessage(null);
            setIsSubmitting(false)
          }, 5000); // hide message after 5 seconds
        }
      );
    e.target.reset();

  };
  return (
    <div className=''>
      <form className='flex flex-col md:flex-row  sm:items-center gap-20  md:gap-72  p-14 rounded-t-lg' ref={form} onSubmit={sendEmail}>

        <div className='flex flex-col  '>
          <h1 className='text-2xl  font-semibold text-black mb-5'>Contact Me</h1>

          {
            isSubmitting &&
            <div className='text-green-600 text-xl fornt-semibold bg-gray-300 px-8 py-2 rounded-lg '>
              {stateMessage}
            </div>
          }
          <div>
            <label htmlFor="email" className="block  text-black">Email</label>
            <input type='email' name='from_email' placeholder='Enter your email' className='w-[35vw]  bg-gray-100 border-2  border-gray-200 outline-none bg-opacity h-12 px-2 rounded-lg text-black shadow-xl ' />
          </div>
          <div className='my-4'>
            <label htmlFor="subject" className="block  text-black">SUbject</label>
            <input type='text' name='subject' placeholder='Enter your Subject' className='w-[35vw]  bg-gray-100 border-2 border-gray-200 outline-none bg-opacity h-12 px-2 rounded-lg text-black shadow-xl' />
          </div>
          <div>
            <label htmlFor="message" className="block  text-black">Message</label>
            <textarea type='text' className=' w-[35vw] h-40 shadow-xl  rounded-lg p-3  bg-gray-100 border-2  border-gray-200 outline-none bg-opacity' placeholder='Enter Your Message Here' name='message' />
          </div>
          <div className='flex justify-center'>
            <button className='px-6 cursor-pointer hover:bg-[#5f5f5f] bg-black/90 justify-end text-sm py-1 rounded-lg w-fit text-center mt-6 text-white '>Submit</button>
          </div>
        </div>
        <div >
          <h1 className='text-3xl font-semibold mb-5 text-black w-96'>
            Say hello and let's work together !
          </h1>

          <p className='flex flex-wrap gap-6 justify-start text-black   items-center'>
            <a href={'mailto:selamyemru6@gmail.com'} target='blank' className='flex gap-6 items-center  text-xl'>
              <FaEnvelope size={15} /></a>
            <a href={'https://github.com/selamyemru6'} target='blank' className='flex gap-6 items-center text-xl'>
              <FaGithub size={15} /></a>
            <a href={'https://www.linkedin.com/in/selam-yemru/'} target='blank' className='flex gap-6 items-center text-xl'>
              <FaLinkedin size={15} /></a>
            <a href={'https://gitlab.com/selamyemru6'} target='blank' className='flex gap-6 items-center text-xl'>
              <FaGitlab size={15} /></a>
            <a href={'tel:+192538401262'} target='blank' className='flex gap-4 items-center text-2xl'>
              <FaPhone size={15} />+192538401262</a>
          </p>
        </div>
      </form>
    </div>


  )
}
export default Contact
