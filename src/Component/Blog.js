import React, { useEffect, useState } from 'react'
import { fetchBlog } from './FetchProject';
import image2 from '../assets/aboutme.jpg'
function Blog() {
  const [blogs, setBlog] = useState([])
  useEffect(() => {
    async function fetch() {
      const response = await fetchBlog()
      setBlog(response)
    }
    fetch()
  }, [])
  return (
    <div className=' flex flex-col  gap-14 '>
      <h1 className=' text-2xl my-5 text-[#321a1a] font-bold '>My Blogs</h1>

      <div className=' grid  grid-cols-1 md:grid-cols-3  gap-10  mr-20'>
        <div className=' bg-gray-200 bg-opacity-50 items-center text-black hover:scale-110   row-span-10 rounded-lg shadow-lg shadow-blue-100'>
          <img src={image2}  alt="post image" className='w-full'/>
        </div>
        <div className='text-black   row-span-10 col-span-2 p-8 flex flex-col gap-4 '>
          <h1 className='text-2xl '>It is a long established fact that a reader will be dis </h1>
          <p>
            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making
          </p>

        </div>
      </div>
      <div >
        <div className='grid grid-cols-2 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-10 '>
          {Array.isArray(blogs) && blogs.map(blog => (
            <div key={blog.$id}  className='bg-[#D9D9D9] hover:border hover:border-[#968d8d] transition-all hover:scale-110 bg-opacity-50 items-center text-black  row-span-8 h-[35vh] p-6 rounded-lg shadow-lg flex flex-col gap-2 max-h-96 hover:shadow-xl hover:bg-[#DBDED3] hover:bg-opacity-50 hover:rotate-3'>
                <img src={blog.Image} alt='post img'/>
                <h1 className='items-center font-semibold '>{blog.Title}</h1>
                <time>{blog.Release_Date}</ time>
              </div>
          ))}
        </div>
      </div>
    </div>
  )
}
export default Blog
