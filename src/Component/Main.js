import React from 'react';
import { useCallback } from "react";
import Particles from "react-particles";
import { loadSlim } from "tsparticles-slim"; 

const Main = () => {
  const particlesInit = useCallback(async engine => {
   
    await loadSlim(engine);
}, []);

const particlesLoaded = useCallback(async container => {
}, []);

return (
  <div className='max-h-screen'>
 <Particles
      id="tsparticles"
      init={particlesInit}
      loaded={particlesLoaded}
      options={{
          background: {
              color: {
                  value: "#fffff",
              },
          },
          fpsLimit: 120,
          interactivity: {
              events: {
                  onClick: {
                      enable: false,
                      mode: "push",
                  },
                  onHover: {
                      enable: true,
                      mode: "repulse",
                  },
                  resize: true,
              },
              modes: {
                  push: {
                      quantity: 3,
                  },
                  repulse: {
                      distance: 200,
                      duration: 0.5,
                  },
              },
          },
          particles: {
              color: {
                  value: "#000000",
              },
              links: {
                  color: "#ff4900",
                  distance: 100,
                  enable: true,
                  opacity: 0.5,
                  width: .5,
              },
              move: {
                  direction: "none",
                  enable: true,
                  outModes: {
                      default: "bounce",
                  },
                  random: false,
                  speed: 3,
                  straight: false,
              },
              number: {
                  density: {
                      enable: true,
                      area: 4000,
                  },
                  value: 80,
              },
              opacity: {
                  value: 0.5,
              },
              shape: {
                  type: "circle",
              },
              size: {
                  value: { min: 1, max: 3 },
              },
          },
          detectRetina: true,
      }}
  />
  </div>
 
);
 
  
};

export default Main;