import React from 'react'
import '../App.css'
import {  FaDownload } from 'react-icons/fa'
import { useState, useEffect } from 'react'
import { fetchResume } from './FetchProject'
import image from '../assets/aboutme.png'
function About() {
  const [resume, setResume] = useState([])
  useEffect(() => {
    async function fetch() {
      const data = await fetchResume()
      setResume(data)
    }
    fetch()
  }, [])
  return (
    <div className=' py-12  fixed top-0 left-0 h-screen  max-w-[25vw] lg:max-w-[20vw]  bg-gray-100 px-11 ' id="About "  >
      <h1 className=' text-2xl md:text-2xl font-semibold font-serif'>Seli_folio</h1>

      <img src={image} alt="me" />
    
        <div className=' flex flex-col rounded-3xl py-8 '>
          <h1 className='text-3xl font-semibold text-gray-600'>Hello, <span className='text-blue-900 font-sans  ' >I'm Selam Yemru</span></h1>
          <h1 className='text-2xl text-gray-600 my-7 font-semibold '>Software Engineer &amp; Graphics Designer</h1>
          <p className='max-w-[50vw]'>I am a web developer with a passion for creating beautiful and functional websites. I have experience working with various technologies, including React, Tailwind CSS, and more.
          </p>
          <div className='mt-2 flex'>
            {
              resume.map(resume =>
                <a key={resume.$id} href={resume.resume} target='_blank' rel="noreferrer" className='rounded-lg hover:bg-gray-200 text-white w-fit px-5 items-center justify-center  hover:text-black bg-black/90  py-1.5 flex gap-2 text-sm  cursor-pointer border shadow-lg ' >
                  <FaDownload size={8} />
                  Resume
                </a>
              )
            }
          </div>
        </div>
      </div>
  )
}
export default About
