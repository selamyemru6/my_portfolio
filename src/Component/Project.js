
import { FaArrowRight, FaCaretDown } from 'react-icons/fa'
import { fetchProject } from './FetchProject'
import { useEffect, useState } from 'react'
function Project() {
  const [project, setProject] = useState([])
  const [showAllProject, setShowAllProject] = useState(true)
  const allProject = async () => {
    setShowAllProject(false)
    const data = await fetchProject()
    setProject(data)
  }
  const showLessProject = async () => {
    setShowAllProject(true)
    const data = await fetchProject()
    setProject(data.slice(0, 6))
  }
  useEffect(() => {
    async function fetch() {
      const response = await fetchProject()
      setProject(response.slice(0, 6))
    }
    fetch()
  }, [])

  return (
    <div className=' mt-[2vw]  flex flex-col gap-14  shadow-2xl shadow-gray-200  p-10'>
      <h1 className='text-2xl font-bold text-[#321a1a]' >My Project</h1>
      <div className='grid grid-cols-1 gap-[3vw] sm:gap-[2vw]  lg:gap-[4vw] sm:grid-cols-2 lg:grid-cols-3 mr-[5vw] '>
        {Array.isArray(project) && project.map(item => (
          <div className='flex flex-col gap-10 items-center  transition-all hover:scale-110 pb-4 bg-[#D9D9D9] bg-opacity-55 shadow-lg hover:shadow-xl  hover:bg-[#DBDED3] hover:bg-opacity-50 rounded-t-lg border' key={item.$id}>
            <img src={item.Image} alt='appwrite img' className='max-h-[14vw]  w-fit mt-0 rounded-lg' />
            <div className='flex flex-col gap-2 items-center justify-center'>
              <h1 className='text-xl font-semibold '>{item.title}</h1>
              <p className='text-center'>{item.Description}</p>
              <a href={item.Link} className='flex gap-4 items-center cursor-pointer'><FaArrowRight />View Project</a>
            </div>
          </div>
        ))}
      </div>
      <div className='flex justify-end mr-32'>
        {showAllProject ? (
          <button
            className="flex  gap-2 items-center px-4 rounded-lg outline-none border-2 border-gray-300 bg-gray-100 w-fit"
            onClick={allProject}
          >
            More Project <FaCaretDown />
          </button>
        ) :
          <button
            className="flex  gap-2 items-center px-4 rounded-lg outline-none border-2 border-gray-300 bg-gray-200 w-fit"
            onClick={showLessProject}
          >
            Show Less <FaCaretDown />
          </button>}

      </div>

    </div>
  )
}
export default Project
