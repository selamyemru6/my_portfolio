import { Databases, Query } from "appwrite";
import { client } from "../lib/apprwite";
const database = new Databases(client)
export async function fetchProject() {
  const project = await database.listDocuments('663b1cb90009db8d7d96', '663b1cd5002f0ffe4661', [Query.orderDesc('$createdAt')])
  const data = project.documents
  return data
}
export async function fetchBlog() {
  const project = await database.listDocuments('663b1cb90009db8d7d96', '663d354600113c0a3e9a', [Query.orderDesc('$createdAt')])
  const data = project.documents
  return data
}
export async function fetchResume() {
  const resume = await database.listDocuments('663b1cb90009db8d7d96', '6641cbfc00377c1bbe36', [Query.orderDesc('$createdAt')])
  const data = resume.documents;
  return data
}
export async function fetchSkill() {
  const skill = await database.listDocuments('663b1cb90009db8d7d96', '6642164b0003fa1dbb9c', [Query.orderDesc('$createdAt')])
  const response = skill.documents;
  return response;
}
//fetch based on category.
export async function skillCatagory() {
  try {
    const response = await database.listDocuments('663b1cb90009db8d7d96', '6642164b0003fa1dbb9c', [
      Query.orderDesc('$createdAt'),
      Query.select(["category"]),

    ])
    const categories = response.documents.map(document => document.category);
    const uniqueCategoriesSet = new Set(categories);
    const uniqueCategories = Array.from(uniqueCategoriesSet);

    return uniqueCategories;
  } catch (error) {
    throw new Error("Failed to get category");
  }


}
//fetch skill with specific category
export async function fetchSkillWithCategory(category) {
  try {
    const response = await database.listDocuments('663b1cb90009db8d7d96', '6642164b0003fa1dbb9c', [
      Query.orderDesc('$createdAt'),
      Query.equal("category", category)])
      const data = response.documents;
       return data;
  } catch (error) {
    throw new Error("Failed to get category");
  }


}

