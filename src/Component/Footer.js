import React from 'react'
import { FaFacebook,FaYoutube,FaTiktok, FaTwitter } from 'react-icons/fa'
import { Link } from 'react-scroll'
function Footer() {
  return (
    <div className='bg-gray-100 flex flex-col gap-2 text-black  border-t-2 border-gray-300 w-full items-center  pl-[18vw] py-[2vh] '>
      <div className='flex gap-3 '>
        <h1 className='mr-4'>Follow me </h1>
        <Link to=''><FaFacebook size={20} /></Link>
        <Link to=''><FaYoutube size={20} /></Link>
        <Link to=''><FaTiktok size={20} /></Link>
        <Link to=''><FaTwitter size={20} /></Link>
      </div>
      
      <h1>&copy; Copy Righ reserved </h1>
    </div>
  )
}

export default Footer
