import React, { useState } from 'react'
import {FaHamburger,FaTimes } from 'react-icons/fa'
import { MdMenu } from 'react-icons/md'
import '../App.css'
import { Link } from 'react-scroll'
function Navbar() {
  const [isToggle, setIsToggle] = useState(false)
  const toggle = () => {
    setIsToggle(!isToggle)
  }
  return (
    <header className='fixed z-40 top-0 w-full  h-10 flex  justify-end  gap-[5vw] py-8 items-center   px-32 '>
      <nav className='outline-none text-xl  text-black   hidden md:block  hover:text-blue-900  font-semibold '>
        <Link  smooth spy to='Skill' className='mr-[1vw]' offset={-80}>Skill</Link>
        <Link  smooth  spy to='Education' className='mr-[1vw]' offset={-80}>Education</Link>
        <Link  smooth spy to='Project' className='mr-[1vw]' offset={-80}>Project</Link>
        <Link  smooth  spy to='Blog' offset={-80}>Blog</Link>
      </nav>
      <div className='md:hidden  flex   text-white]'>
        <button onClick={toggle}>
          {isToggle?<FaTimes/>:<MdMenu size={30} className='-mr-[40vw]'/>} 
          </button>
        {isToggle && (
          <ul className='flex flex-col gap-3 text-xl mix-w-[20vw] px-4  -mr-24 bg-gray-200  text-black  py-5 rounded-lg text-center  fixed right-28  top-10 hover:text-blue-900  '>
            <Link  smooth to='Project'>Project</Link>
            <Link  smooth to='Skill'>Skill</Link>
            <Link  smooth to='Education'>Education</Link>
            <Link  smooth to='Blog'>Blog</Link>
          </ul>
        )}
      </div>
    </header>



  )
}

export default Navbar
